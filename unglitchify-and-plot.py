import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as mticker
import re
import sys


"""
  Read a log file from the IOC, having lines like this:
  "LabS-MCAG:MC-MCU-07:DCtEL1252NSec 2020-05-26 13:32:16.926089 872013454  "
  The "DCtEL1252NSec" PV gets recorded, when there is a 0 -> 1 transition
  on the 1Hz pulse, connected to the EL1252.
  The 1252 latches the time of the transition,
  and that is what we want to measure and plot.
  Note that there are sometimes 2 (or 3) transtions inside a second,
  but we feed only 1. Where the other ones come from, need to be
  investigated (noise on the line?)
  This is where the ung-litch function comes in:
  If we have more than one transition within the same seocnd,
  ignore both.
"""

debug = False


def main(argv=None):
    global debug
    if not argv:
        argv = sys.argv
    dontUseGlitchDetector = False
    output_file_name = None
    argvidx = 1
    input_file_name = argv[argvidx]
    argvidx += argvidx
    if debug:
        print("len(argv)=%u argvidx=%u" % (len(argv), argvidx))
    if len(argv) > argvidx:
        output_file_name = argv[argvidx]
    argvidx += argvidx

    fid = open(input_file_name)

    lines = fid.readlines()

    # Collect date into an X- and Y- array
    x = []
    y = []
    oldSeconds = None
    quran_x_time = None
    quran_yvalue = None

    RE_MATCH_IOC_PV_NOT_FOUND = re.compile(
        r".*( \*\*\* Not connected \(PV not found\))"
    )
    RE_MATCH_IOC_PV_DISCONNECTED = re.compile(
        r".*( \*\*\* disconnected)"
    )
    RE_MATCH_INVALID_UNDEFINED = re.compile(
        r".*(INVALID|<undefined>).*"
    )
    RE_MATCH_DIGITS = re.compile(
        r"(\d+)"
    )
    RE_MATCH_YYMMDD = re.compile(
        r"(.*:.*)"
    )

    # Regular expression to split the line into its components -
    # Do a basic sanity test
    #                                    name      date        time      raw
    RE_MATCH_IOC_LOG_LINE = re.compile(r"(\S+)\s+([0-9-]+)\s+([0-9:.]+)\s+(.+)")
    RE_MATCH_PTPOFFSET_OR_STATE = re.compile(r".*:(PTPOffset|PTPState).*\d$")

    #        year      month     day       hour     minute    second     nsec
    RE_MATCH_RAW_SDCTEL1252 = re.compile(
        r"([0-9-]+)-([0-9-]+)-([0-9-]+)-([0-9-]+):([0-9-]+):([0-9-]+)\.([0-9-]+);"
    )

    for i in range(0, len(lines)):
        try:
            yvalue = None
            line = lines[i]
            match_ioc_pv_not_found = RE_MATCH_IOC_PV_NOT_FOUND.match(line)
            if match_ioc_pv_not_found != None:
                continue
            match_ioc_pv_disconnected = RE_MATCH_IOC_PV_DISCONNECTED.match(line)
            if match_ioc_pv_disconnected != None:
                continue

            match_ioc_log_line = RE_MATCH_IOC_LOG_LINE.match(line)
            if match_ioc_log_line == None:
                print("re_match == None line=%s" % (line))
                sys.exit(1)

            pvname = match_ioc_log_line.group(1)
            date = match_ioc_log_line.group(2)
            time = match_ioc_log_line.group(3)
            raw = match_ioc_log_line.group(4)
            if debug:
                print(
                    "match_ioc_log_line=%s pvname=%s date=%s time=%s raw=%s "
                    % (match_ioc_log_line, pvname, date, time, raw)
                )
            match_invalid_undefined = RE_MATCH_INVALID_UNDEFINED.match(raw)
            if match_invalid_undefined != None:
                if debug:
                    print(
                        "match_invalid_undefined=%s raw=%s"
                        % (match_invalid_undefined, raw)
                    )
                continue
            #LabS-MCAG:MC-MCU-07:DClockEL1252    2020-12-01 13:08:45.856  660143325697997180 
            match_yymmdd = RE_MATCH_YYMMDD.match(raw)
            match_digits = RE_MATCH_DIGITS.match(raw)
            if debug:
                print(
                    "match_yymmdd=%s match_digits=%s raw=%s"
                    % (match_yymmdd, match_digits, raw)
                )
            if match_yymmdd != None:
                # 2020-05-26-13:32:16.872013454;
                nsecSplit = raw.split(".")
                if debug:
                    print(
                        "len(nsecSplit)=%d" % (len(nsecSplit))
                    )
                if len(nsecSplit) == 2:
                    match_raw_sdctEL1252 = RE_MATCH_RAW_SDCTEL1252.match(raw)
                    if debug:
                        print(
                            "match_raw_sdctEL1252=%s"
                            % (match_raw_sdctEL1252)
                    )
                    sdct_year = match_raw_sdctEL1252.group(1)
                    sdct_month = match_raw_sdctEL1252.group(2)
                    sdct_day = match_raw_sdctEL1252.group(3)
                    sdct_hour = match_raw_sdctEL1252.group(4)
                    sdct_minute = match_raw_sdctEL1252.group(5)
                    sdct_sec = match_raw_sdctEL1252.group(6)
                    sdct_nsec = match_raw_sdctEL1252.group(7)

                    if debug:
                        print(
                            "match_raw_sdctEL1252=%s sdct_year=%s sdct_month=%s sdct_day=%s sdct_hour=%s sdct_minute=%s sdct_sec=%s sdct_nsec=%s"
                            % (
                                match_raw_sdctEL1252,
                                sdct_year,
                                sdct_month,
                                sdct_day,
                                sdct_hour,
                                sdct_minute,
                                sdct_sec,
                                sdct_nsec,
                            )
                        )
                    nsec = sdct_nsec
                    yvalue = float(nsec) / 1000.0
                elif len(nsecSplit) == 1:
                    nsec = nsecSplit[0]
                    yvalue = float(nsec) / 1000.0
            elif match_digits  != None:
                nsec = int(match_digits.group(1))
                if debug:
                    print(
                        "match_digits=%s raw=%s nsec=%d"
                        % (match_digits, raw, nsec)
                    )
                yvalue = float(nsec % 1000000000.0)

            if yvalue is None:
                print("yvalue is None line=%s" % (line))
                print("  raw=%s" % (raw))
                print("  len(nsecSplit)=%s nsec=%s" % (len(nsecSplit), nsec))
                print("  nsec=%s type(nsec=%s)" % (nsec, type(nsec)))
                sys.exit(1)

            x_time = datetime.datetime.fromisoformat(date + " " + time)
            # Split off the fraction of a second.
            x_time_sec_res = str(x_time).split(".")[0]
            if debug:
                print("x_time_sec_res=%s oldSeconds=%s" % (x_time_sec_res, oldSeconds))
            match_ptpoffset_or_state = RE_MATCH_PTPOFFSET_OR_STATE.match(line)
            if debug:
                print(
                    "match_ptpoffset_or_state=%s pvname=%s"
                    % (match_ptpoffset_or_state, pvname)
                )

            if dontUseGlitchDetector or (match_ptpoffset_or_state != None):
                # There is no need to suppress 2 PTPOffset measurements
                # done within the same second.
                x.append(x_time)
                y.append(yvalue)
                continue

            if oldSeconds != None:
                if debug:
                    print(
                        "x_time=%s x_time_sec_res=%s oldSeconds=%s"
                        % (x_time, x_time_sec_res, oldSeconds)
                    )
                MAX_DELTA = 20000  # 20 usec
                if quran_yvalue is not None and abs(quran_yvalue - yvalue) > MAX_DELTA:
                    # The line has a '\n' at its end
                    print("glitch old_line %s      new_line  %s" % (quran_line, line))
                    # Ignore those valuse:
                    x_time = None
                    yvalue = None
                    quran_x_time = None
                    quran_yvalue = None
                    oldSeconds = None
                if (
                    x_time != None
                    and yvalue != None
                    and quran_x_time != None
                    and quran_yvalue != None
                ):
                    if debug:
                        print(
                            "append quran_x_time=%s quran_yvalue=%s"
                            % (quran_x_time, quran_yvalue)
                        )
                    x.append(quran_x_time)
                    y.append(quran_yvalue)
                # prepare for next round
                quran_x_time = x_time
                quran_yvalue = yvalue

            oldSeconds = x_time_sec_res
            quran_line = line

        except ValueError:
            print("lines[i]=%s" % (lines[i]))
            print("lines[i].split=%s" % (lines[i].split(" ")))
            sys.exit(1)

    fid.close()
    xnp = np.array(x)
    ynp = np.array(y)

    plt.plot(xnp, ynp)
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m-%d %H:%M:%S"))
    plt.gca().yaxis.set_major_formatter(mticker.FormatStrFormatter("%f usec"))

    for txt in plt.gca().xaxis.get_majorticklabels():
        txt.set_rotation(270)

    plt.tight_layout()
    plt.title(output_file_name)
    if output_file_name is not None:
        if debug:
            print("output_file_name=%s" % (output_file_name))
        plt.savefig(output_file_name)
    plt.show()


if __name__ == "__main__":
    sys.exit(main(sys.argv))
